Con anterioridad al 18 de noviembre me enviaréis las propuestas que habéis redactado vía classroom para que yo os confirme si es viable o no
Cuando vuestra propuesta esté validada se realizará una exposición oral para explicar a la clase cuál es vuestro proyecto, por qué se ha elegido, las razones de la exclusión del resto de las ideas y la descripción del sistema elegido
La fecha límite para entregar el proyecto inicial es el 27 de noviembre
